" coloring
syntax enable
colorscheme monokai 
hi Search ctermfg=Black ctermbg=Yellow cterm=NONE guifg=NONE guibg=NONE gui=NONE

" indentation
set tabstop=4 
set softtabstop=4
set expandtab

" general
set number
set showcmd
filetype indent on
set shiftwidth=4
set autoindent
set smartindent
set cindent
set laststatus=2
set noshowmode
set hlsearch
set completeopt -=preview
set encoding=utf-8

" plugins
execute pathogen#infect()

if !has('gui_running')
    set t_Co=256
endif


" shortcuts 
map <C-n> : NERDTreeToggle<CR>
:nnoremap <silent> <Space> :nohlsearch<Bar>:echo<CR>

" buffer navigation bindings
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

autocmd FileType make setlocal noexpandtab

" YCM setup for python envs:
let g:ycm_python_interpreter_path = ''
let g:ycm_python_sys_path = []
let g:ycm_extra_conf_vim_data = [
  \  'g:ycm_python_interpreter_path',
  \  'g:ycm_python_sys_path'
  \]
let g:ycm_global_ycm_extra_conf = '~/git/dotfiles/vim/global_extra_conf.py'
