#!/bin/bash
DISTRO=$1

echo "Vim Setup Started..."

# Theme
mkdir -p "$HOME/.vim/colors"
ln -sf "$HOME/git/dotfiles/vim/monokai.vim" "$HOME/.vim/colors/"

if [ $DISTRO != "arch" ]
then
    echo "Installing Prerequisites..."
    sudo apt install build-essential cmake python-dev python2.7-dev python3-dev git
fi

echo "Installing Pathogen..."
mkdir -p ~/.vim/autoload
mkdir -p ~/.vim/bundle

curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim

# LightLine
echo "Installing LightLine..."
git clone https://github.com/itchyny/lightline.vim ~/.vim/bundle/lightline.vim

# YouCompleteMe
#echo "Installing YouCompleteMe..."
#cd ~/.vim/bundle
#git clone https://github.com/Valloric/YouCompleteMe.git
#cd YouCompleteMe
#git submodule update --init --recursive

# install stuff needed for this installation manually
#python install.py --all
