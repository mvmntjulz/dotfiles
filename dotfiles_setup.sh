#!/bin/bash

DISTRO=$1

# bash
echo alias "ll=\"ls -lh --color=auto\"" >> "$HOME/.bashrc"

# profile
echo export BROWSER=/usr/bin/firefox >> "$HOME/.profile"
echo export EDITOR=/usr/bin/vim >> "$HOME/.profile"
echo export QT_QPA_PLATFORMTHEME=\"qt5ct\" >> "$HOME/.profile"
echo export GTK2_RC_FILES=\"$HOME/.gtkrc-2.0\" >> "$HOME/.profile"

# SSH
#mkdir -p "$HOME/.ssh"
#ln -sf "$HOME/git/dotfiles/ssh/config" "$HOME/.ssh/" 
#chmod "700" "$HOME/.ssh"
#chmod "644" "$HOME/.ssh/sshkey.pub"
#chmod "600" "$HOME/.ssh/sshkey"

# VIM
dotfiles=(".vimrc" ".ideavimrc")
dir="${HOME}/git/dotfiles/vim"

for df in "${dotfiles[@]}"; do
    ln -sf "${dir}/${df}" "${HOME}"
done

# passgen
#mkdir "$HOME/passgen"
#ln -s "$HOME/git/dotfiles/passgen/pg.py" "$HOME/passgen/pg.py"
#sudo cp "$HOME/git/dotfiles/passgen/pg" "/usr/local/bin"
#yay -S xclip

# i3
ln -sf "$HOME/git/dotfiles/i3/config" "$HOME/.i3/config" 

# rxvt
# for font resizing in rxvt-terminal
ln -sf "$HOME/git/dotfiles/X/.Xresources" "$HOME/" 
if [ $DISTRO == "arch" ]
then
    yay -S urxvt-resize-font-git
fi

# other stuff
yay -S pulseaudio pulseaudio-alsa
yay -S reshift
yay -S flameshot
