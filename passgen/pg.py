#!/usr/bin/python3

import string
import operator
import sys
import subprocess
import platform
import os
from string import digits


spoken_numbers = {0:'zero', 
                  1: 'one', 
                  2: 'two', 
                  3:'three', 
                  4: 'four', 
                  5: 'five', 
                  6: 'six', 
                  7: 'seven', 
                  8: 'eight', 
                  9: 'nine'}


def digit2str(input_str):
    for key, value in spoken_numbers.items():
        input_str = input_str.replace(str(key), value)

    return input_str 


def main():
        if len(sys.argv) < 2:
            p = input("Service-Name:")
        else:
            p = sys.argv[1]

        p = digit2str(p) # in order to turn service2 into servicetwo
        p = p.upper()

        w="JULIEN"
        f=
        c=
        v = dict()
        
        if len(c) != 10 or len(f) != 26:
                exit()
        pw = p + w
        for i, l in enumerate(string.ascii_uppercase):
                v[l] = i
                
        a = list()
        for l in pw:
                a.append(f[v[l]])
                
        result = ""
        b = 0
        for i, ai in enumerate(a):
                b = c[(a[-1] + a[0]) % len(c) if not i else (b + ai) % len(c)]
                result += str(b)
        result = "Pw!" + result
    
        if platform.system() == "Windows":
            os.system('echo|set/p="{}" | clip'.format(result[0:16]))
        else:
            ps = subprocess.Popen(["printf", result[0:16]], stdout=subprocess.PIPE)
            subprocess.call(["xclip","-selection", "clipboard"], stdin=ps.stdout)
        
if __name__ == "__main__":
    main()
